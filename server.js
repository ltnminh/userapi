// PACKAGES 
var express = require('express');
var app = express();
var path = require('path');

var mongoose = require('mongoose');

var bodyParser = require('body-parser');

var morgan = require('morgan');


var port = process.env.PORT || 3000; 


// Connect to database 

mongoose.connect('mongodb://localhost:27017/sap');

// Use model 

var User = require('./model/user');

// APP CONFIGURATION 

app.use(bodyParser.urlencoded({extended: true }));
app.use(bodyParser.json());

app.use(function(req,res,next){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods','GET,POST');
	res.setHeader('Access-Control-Allow-Headers','X-Requested-With,content-type, \ Authoriazation');
	next();
});

app.use(morgan('dev'));




// ROUTES 

// basic route
app.get('/',function(req,res){
	res.sendFile(path.join(__dirname + '/index.html'))
}); 


// API Routes 
var apiRouter = express.Router();

apiRouter.use(function(req,res,next){
	console.log('Someone just connected to the API ');

	next();
})

// Basic Routes 

apiRouter.get('/',function(req,res){
	res.json({message: 'Hello, World' });
})

// For /users routes ( GET , POST )
 
apiRouter.route('/users') 
 	.post(function(req,res) { 
 		var user = new User();

 		user.email             = req.body.email;
 		user.password          = req.body.password;
 		user.tel               = req.body.tel;
 		user.avatar_url        = "";
 		user.friendlist        = "";
 		user.activity_wishlist = "";
 		user.permission_name = "[share,post]";
 		user.attend_activity_id = "";
 		user.cash_amount = "0";
 		user.del_flag = "0";

 		user.save( function(err) { 
 			if (err) { 
 				if (err.code == 11000 )
 					return res.json( { success: false, message:'A user with that email already exists. '});
 				else 
 					return res.send(err);
 			}

 			res.json({ message: 'User created!' });

 		});
 	})

 	.get(function(req,res){
 		User.find(function(err,users){
 			if (err) res.send(err);
 			res.json(users);
 		})
 	})

// For /user_id routes 
apiRouter.route('/users/:user_id')
	.get(function(req,res){
		User.findById(req.params.user_id, function(err,user){
			if (err) res.send(err);

			res.json(user);
		})
	})

	.put(function(req,res){
		User.findById(req.params.user_id,function(err,user){
			if (err) res.send(err);

			
			if (req.body.password)    user.password    = req.body.password;
			if (req.body.tel)         user.tel         = req.body.tel;
			if (req.body.avatar_url)  user.avatar_url  = req.body.avatar_url;
			if (req.body.cass_amount) user.cass_amount = req.body.cass_amount;


			user.save(function(err){
				if (err) res.send(err);

				res.json({ message: 'User updated!' });
			});
		})
	})

	.delete(function(req,res){
		User.remove({ _id: req.params.user_id },function(err,user){
			if (err) return res.send(err); 
			res.json({message: 'Successfully' })
		})
	});

// REGISTER ROUTES 

app.use('/api',apiRouter);



// START THE SERVER 

app.listen(port);
console.log(port + ' is on')