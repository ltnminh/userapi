var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({ 
	email :    { type : String , required : true , index : { unique : true }},
	password :    { type : String , required : true , select : true },
	tel :                 String, 
	avatar_url :          String,
	friendlist :          String,
	activity_wishlist :   String,
	permission_name :     String,
	attend_activity_id :  String,
	cash_amount: String,
	created_time : { type: Date, default: Date.now } ,
	updated_time : { type: Date, default: Date.now } ,  
	del_flag : String
},{ collection: 'user_info' });

UserSchema.pre('save',function(next) {
	var user = this; 

	if(!user.isModified('password')) return next();

	bcrypt.hash(user.password,null,null,function(err, hash) {
		if (err) return next(err);

		user.password = hash;
		next();
	})
})

UserSchema.methods.comparePassword = function(password) {
	var user = this; 
	return bcrypt.compareSync(password,user.password);
}

module.exports = mongoose.model('User',UserSchema);